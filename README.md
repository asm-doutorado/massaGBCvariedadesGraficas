# Sobre o Projeto

Essa projeto de pesquisa consiste em investigar a existência de uma fórmula
explícita para as massas GBC (Gauss-Bonnet-Chern) na classe das variedades
gráficas assintoticamente euclidianas, de codimensão alta no espaço euclidiano,
e suas possíveis aplicações às versões das desigualdades da
massa positiva e de Penrose para as massas GBC.

Nossa abordagem é inspirada no trabalho de Feliciano Vitório e Heudson Mirandola
sobre a *[massa ADM das varidades gráficas de codimensão alta no espaço euclidiano](http://arxiv.org/abs/1304.3504)*.

# Resultados

Os resultados alcançados nesse projeto se encontram publicados no artigo
*[The Gauss-Bonnet-Chern mass of higher codimension graphical manifolds](https://arxiv.org/abs/1509.00456)*
([arXiv:1509.00456 [math.DG]](https://arxiv.org/abs/1509.00456)).